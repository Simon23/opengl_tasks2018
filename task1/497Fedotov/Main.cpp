//
// Created by Simon Fedotov on 14.03.2018.
//

#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <iostream>
#include <vector>
#include <fstream>


class PerfectMaze : public Application {
public:
    PerfectMaze(std::string _fileWithMaze) {
        std::ifstream fileIn(_fileWithMaze);

        fileIn >> isNeedCeiling >> isInside >> height >> width;
        maze.resize(height, std::vector<int>(width));
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                fileIn >> maze[i][j];
            }
        }
    }
    PerfectMaze(int height, int width, bool isInside, bool isNeedCeiling) {
        this -> height = height;
        this -> width = width;
        this -> isInside = isInside;
        this -> isNeedCeiling = isNeedCeiling;

        maze.resize(height, std::vector<int>(width));
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                std::cin >> maze[i][j];
            }
        }
    }

    MeshPtr buildWall(int kind) {
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec3> normals;
        std::vector<glm::vec2> texcoords;


        // 1
        vertices.push_back(glm::vec3(0.0, 0.0, 0.0));
        vertices.push_back(glm::vec3(0.0, 0.0, 1.0));
        if (kind == 2) {
            vertices.push_back(glm::vec3(1.0, 0.0, 0.0));
        }
        if (kind == 1) {
            vertices.push_back(glm::vec3(0.0, -1.0, 0.0));
        }

        if (kind == 2) {
            normals.push_back(glm::vec3(0.0, 1.0, 0.0));
            normals.push_back(glm::vec3(0.0, 1.0, 0.0));
            normals.push_back(glm::vec3(0.0, 1.0, 0.0));
        }

        if (kind == 1) {
            normals.push_back(glm::vec3(1.0, 0.0, 0.0));
            normals.push_back(glm::vec3(1.0, 0.0, 0.0));
            normals.push_back(glm::vec3(1.0, 0.0, 0.0));
        }

        texcoords.push_back(glm::vec2(0.0, 0.0));
        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(1.0, 0.0));

        // 2
        vertices.push_back(glm::vec3(0.0, 0.0, 1.0));
        if (kind == 2) {
            vertices.push_back(glm::vec3(1.0, 0.0, 1.0));
            vertices.push_back(glm::vec3(1.0, 0.0, 0.0));
        }
        if (kind == 1) {
            vertices.push_back(glm::vec3(0.0, -1.0, 1.0));
            vertices.push_back(glm::vec3(0.0, -1.0, 0.0));
        }

        if (kind == 2) {
            normals.push_back(glm::vec3(0.0, 1.0, 0.0));
            normals.push_back(glm::vec3(0.0, 1.0, 0.0));
            normals.push_back(glm::vec3(0.0, 1.0, 0.0));
        }

        if (kind == 1) {
            normals.push_back(glm::vec3(1.0, 0.0, 0.0));
            normals.push_back(glm::vec3(1.0, 0.0, 0.0));
            normals.push_back(glm::vec3(1.0, 0.0, 0.0));
        }

        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(1.0, 1.0));
        texcoords.push_back(glm::vec2(1.0, 0.0));

        //----------------------------------------

        DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

        DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

        DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

        MeshPtr mesh = std::make_shared<Mesh>();
        mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
        mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
        mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
        mesh->setPrimitiveType(GL_TRIANGLES);
        mesh->setVertexCount(vertices.size());

        return mesh;
    }


    void makeScene() override {
        floor = makeGroundPlane(width, 2, glm::vec3(0.0, 0.0, 1.0));
        floor->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
        ceiling = makeGroundPlane(width, 2, glm::vec3(0.0, 0.0, -1.0));
        ceiling->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 1.0f)));

        MeshPtr mesh;
        for (int i = 0; i < maze.size(); ++i) {
            for (int j = 0; j < maze[0].size(); ++j) {
                if (maze[i][j] != 0) {
                    mesh = buildWall(maze[i][j]);
                    mesh->setModelMatrix(
                            glm::translate(
                                    glm::mat4(1.0f),
                                    glm::vec3(
                                        float(j) - float(width)/2.0f,
                                        -float(i) + float(height)/2.0f,
                                        0.0f
                                    )
                            )
                    );
                    walls.push_back(mesh);
                }
            }
        }
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();

        _shader = std::make_shared<ShaderProgram>(
                "/Users/semenfedotov/Desktop/MIPT/8Semester/comp_graphics/my_cg_tasks/task1/497Fedotov/497FedotovData/shader.frag"
                ,                 "/Users/semenfedotov/Desktop/MIPT/8Semester/comp_graphics/my_cg_tasks/task1/497Fedotov/497FedotovData/shaderNormal.vert");

    }

    void draw() override {
        Application::draw();

        int _width, _height;
        glfwGetFramebufferSize(_window, &_width, &_height);

        glViewport(0, 0, _width, _height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Устанавливаем шейдер.
        _shader->use();
        //Устанавливаем общие юниформ-переменные
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //Рисуем pol
        _shader->setMat4Uniform("modelMatrix", floor->modelMatrix());
        floor->draw();


        //Рисуем potolok
        if (isNeedCeiling) {
            _shader->setMat4Uniform("modelMatrix", ceiling->modelMatrix());
            ceiling->draw();
        }

        // steny
        for (int i = 0; i < walls.size(); ++i) {
            _shader->setMat4Uniform("modelMatrix", walls[i]->modelMatrix());
            walls[i]->draw();
        }
    }

private:
    bool isInside;
    bool isNeedCeiling;
    int height;
    int width;
    ShaderProgramPtr _shader;
    MeshPtr floor;
    MeshPtr ceiling;
    std::vector<MeshPtr> walls;
    std::vector<std::vector<int>> maze;

    CameraMoverPtr cameraMover;
    CameraMoverPtr orbitMover;
};

int main() {
    PerfectMaze myPerfectMaze = PerfectMaze("497FedotovData/test_maze.txt");
    myPerfectMaze.start();
    return 0;
}