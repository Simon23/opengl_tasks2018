include_directories(common)


set(SRC_FILES
        Main.cpp
        common/Application.cpp
        common/Camera.cpp
        common/Mesh.cpp
        common/ShaderProgram.cpp
        common/DebugOutput.cpp
        )

set(HEADER_FILES
        common/Application.hpp
        common/Camera.hpp
        common/Mesh.hpp
        common/ShaderProgram.hpp
        common/DebugOutput.h
        )



MAKE_TASK(497Fedotov 1"${SRC_FILES}")
