/*
Получает на вход интеполированный цвет фрагмента и копирует его на выход.
*/

#version 410

in vec4 color;

out vec4 fragColor;

void main()
{
    fragColor = color;
}
